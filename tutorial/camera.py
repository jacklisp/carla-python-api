#!/usr/bin/env python

import glob
import os
import sys
import time

try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla
import random
import numpy as np
import cv2



IM_WIDTH = 640
IM_HEIGHT = 480

timestamp = []

def process_img(image):
    # print(dir(image))
    timestamp.append(image.timestamp)
    if len(timestamp)>=2:
        mwaitkey = int(round((timestamp[-1]-timestamp[-2]) *1000,1))
        print(mwaitkey)
        # print("mwaitKey",timestamp[-1],timestamp[-2],(timestamp[-1]-timestamp[-2])*100)
    else:
        mwaitkey = 100
    print("timestamp for image:",image.timestamp)
    # print("Now timestamp list:",timestamp)
    # print(np.asarray(image.raw_data).shape)
    # return 0
    # print(image.raw_data.size) # check how many attribute image have
    # i = np.asarray(image.raw_data)
    # image.release()
    i = np.array(image.raw_data)
    i2 = i.reshape((IM_HEIGHT,IM_WIDTH,4))# r,g,b,a(alpha transparency)
    i3 = i2[ :, :, :3]
    # print(i3)
    # print("imshow front")
    # time.sleep(0.001)
    # img = 
    cv2.imshow('Front camera',i3)

    # print("imshow back")

    cv2.waitKey(100) # still have problems with waitKey
    # res = cv2.waitKey(mwaitkey)
    # print(res)
    # if cv2.waitKey(33) == ord('a'):
    #    print("pressed a")
    # cv2.destroyWindow("Front camera")
    # cv2.destroyAllWindows() 
    # print("waitKey back")
    # i4=i3/255.0
    return i3/255.0 #i3/255.0
    

actor_list = []
try:
    # STEP 1: World and client
    client = carla.Client("localhost",2000)
    client.set_timeout(2.0)
    world = client.get_world()

    settings = world.get_settings()
    print(settings.fixed_delta_seconds)
    # settings.fixed_delta_seconds = 0.1
    # settings.synchronous_mode = True
    # world.apply_settings(settings)


    # STEP 2: Actor and blueprints
    blueprint_library = world.get_blueprint_library()

    bp = blueprint_library.filter("model3")[0] # get model3 Tesel
    print(bp)

    # spawn_point = random.choice(world.get_map().get_spawn_points())
    spawn_point = carla.Transform(carla.Location(x=0, y=-150, z=0.275307), carla.Rotation(pitch=0.000000, yaw=92.004189, roll=0.000000))
# Transform(Location(x=-85.144127, y=144.538834, z=0.275307), Rotation(pitch=0.000000, yaw=89.787674, roll=0.000000))

    print("spawn_point show:",spawn_point)
    vehicle = world.spawn_actor(bp, spawn_point)
    #vehicle.set_autopilot(True) # defalut: False
    vehicle.apply_control(carla.VehicleControl(throttle = 1.0,steer = 0.0))
    actor_list.append(vehicle)
    
    # STEP 3: sensor
    camera_bp = blueprint_library.find('sensor.camera.rgb')
    camera_bp.set_attribute("image_size_x",f"{IM_WIDTH}")
    camera_bp.set_attribute("image_size_y",f"{IM_HEIGHT}")
    camera_bp.set_attribute("fov","110")
    # camera_bp.set_attribute("sensor_tick","1.0")

    spawn_point = carla.Transform(carla.Location(x=2.5,z=2))
    
    camera = world.spawn_actor(camera_bp,spawn_point,attach_to=vehicle)
    actor_list.append(camera)
    camera.listen(lambda image: 
        {
            # print("process_img front"),
            process_img(image)
            # print("process_img back"),
            # print(image.frame),
            # print("print image frame front")
        })

    # cc = carla.ColorConverter.LogarithmicDepth
    # camera.listen(lambda image: 
    #     {
    #         image.save_to_disk('_out/%06d.png' % image.frame),
    #         print(image.frame)
    #     })
    # lidar_bp = blueprint_library.find('sensor.lidar.ray_cast')

    time.sleep(10.0)

finally:
    for actor in actor_list:
        actor.destroy()
    print("All actors cleaned up!")