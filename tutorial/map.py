"""
此代码主要用于对opendrive map消息格式的探索
"""

import glob
import os
import sys

# ==============================================================================
# -- Find CARLA module ---------------------------------------------------------
# ==============================================================================
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

# ==============================================================================
# -- Add PythonAPI for release mode --------------------------------------------
# ==============================================================================
try:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/carla')
except IndexError:
    pass

import carla
import math
import numpy as np
import pandas as pd

import hydra
from carla_birdeye_view import BirdViewProducer, BirdViewCropType, PixelDimensions
import cv2
def get_actor(world, name):
    vehicles_list = world.get_actors().filter('vehicle.*')
    for v in vehicles_list:
        # print(v.get_transform())
        if v.attributes['role_name'] == name:
            hero_vehicle = v # Actor
            return v
    print("didn't find the actor with name", name)
    return None

@hydra.main(config_path="config", config_name="carlasetting")
def main(config):
    try:
        # Thing in Carla
        client = carla.Client('localhost', config.localhost)
        client.set_timeout(config.timeout)
        # world = client.load_world(config.loadtown)
        world = client.get_world()
        CarlaMap = world.get_map()
        # with open(config.opmapfile) as od_file:
        #     opendrive = od_file.read()
        # CarlaMap = carla.Map('map', opendrive)
        birdview_producer = BirdViewProducer(
            CarlaMap,
            target_size=PixelDimensions(width=224, height=224),
            render_lanes_on_junctions=True,
            pixels_per_meter=4,
            crop_type=BirdViewCropType.FRONT_AND_REAR_AREA,
        )
        while True:
            agent_tf = get_actor(world,'hero').get_transform()
            bbxy = np.load(config.bbxy)
            birdview = birdview_producer.produce(agent_tf, bbxy)
            rgb = BirdViewProducer.as_rgb(birdview)
            bgr = cv2.cvtColor(rgb, cv2.COLOR_BGR2RGB)
            cv2.imshow("BirdView RGB", bgr)
            key = cv2.waitKey(10) & 0xFF
            if key == 27:  # ESC
                break
        print('Done')

    finally:
        cv2.destroyAllWindows()
        print("DESTORY CV Windows....")
        print('Destory things from this scripts')

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print(' - Exited by user.')